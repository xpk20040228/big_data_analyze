import random

name_str = "駱俊翔、陳佳玲、鄭志丹、鄭淑玲、藍婉菁、李仰夫、王美華、霍秀蓉、陳建佑、林俊賢、馮惠雯"
names = name_str.split('、')

print("學生姓名\t國文\t英文\t數學\t總分")
for name in names:
    chinese = random.randint(0, 99)
    english = random.randint(0, 99)
    math = random.randint(0, 99)
    total = chinese + english + math
    print(f"{name}\t{chinese:2d}\t{english:2d}\t{math:2d}\t{total:3d}")
