import calendar

def month_calendar(chinese_year, month):
    # 將民國年份轉換為西元年份
    year = chinese_year + 1911

    # 建立一個月曆物件
    cal = calendar.monthcalendar(year, month)

    # 印出月曆的標頭
    print('日 一 二 三 四 五 六')

    # 印出每一週的日期
    for week in cal:
        for day in week:
            # 如果日期為 0，表示該天不在該月份內，印出空白
            if day == 0:
                print('   ', end=' ')
            else:
                print(f'{day:2}', end=' ')
        print()

# 呼叫函式，印出 113 年 2 月的月曆
month_calendar(113, 2)
