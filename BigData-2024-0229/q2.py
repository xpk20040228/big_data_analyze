import random

name_str = "駱俊翔、陳佳玲、鄭志丹、鄭淑玲、藍婉菁、李仰夫、王美華、霍秀蓉、陳建佑、林俊賢、馮惠雯"
names = name_str.split('、')

html = "<table border=1>\n"
html += "<tr><th>學生姓名</th><th>國文</th><th>英文</th><th>數學</th><th>總分</th></tr>\n"

for name in names:
    chinese = random.randint(0, 99)
    english = random.randint(0, 99)
    math = random.randint(0, 99)
    total = chinese + english + math
    html += f"<tr><td>{name}</td><td>{chinese}</td><td>{english}</td><td>{math}</td><td>{total}</td></tr>\n"

html += "</table>"

# Write the HTML string to a file
with open("111590003.html", "w", encoding="utf-8") as file:
    file.write(html)

print("HTML file has been written.")
