def get_index(scores, subject_no):
    # 使用 enumerate 函式獲得每個元素的索引和值
    indexed_scores = list(enumerate([score[subject_no] for score in scores]))
    # 根據成績進行排序，並只保留索引
    sorted_indexes = [index for index, _ in sorted(indexed_scores, key=lambda x: x[1])]
    return sorted_indexes

# 假設 scores 是一個二維串列，其中每個元素是一個包含學生姓名和三科成績的串列
scores = [
    ["駱俊翔", 72, 84, 85],
    ["陳佳玲", 43, 13, 33],
    ["鄭志丹", 77, 68, 4],
    ["鄭淑玲", 90, 53, 42],
    ["藍婉菁", 76, 81, 56],
    ["李仰夫", 49, 6, 91],
    ["王美華", 39, 4, 36],
    ["霍秀蓉", 62, 41, 62],
    ["陳建佑", 64, 19, 55],
    ["林俊賢", 16, 80, 39],
    ["馮惠雯", 83, 38, 39]
]

# 計算每個學生的總分並添加到對應的串列中
for score in scores:
    score.append(sum(score[1:]))

# 獲得總分的索引排序
total_score_indexes = get_index(scores, -1)

# 將排序結果附加到每筆資料的最後面
for i, score in enumerate(scores):
    score.append(total_score_indexes.index(i) + 1)

# 輸出結果
for score in scores:
    print(score)
