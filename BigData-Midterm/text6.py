import numpy as np

ary = np.array([5, 13, 3, 5, 11, 8, 13, 9, 10, 13, 3, 9, 8, 11, 7, 9, 13, 8])
A = ary.reshape(6, 3)
B = ary.reshape(3, 6)
C = np.dot(A, B)

print(C)
