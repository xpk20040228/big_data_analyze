import pandas as pd

# Load the data
df = pd.read_csv('112_students.csv')

# Filter the data
df = df[(df['縣市名稱'] == '19 臺中市') & 
        (df['科系名稱'].str.contains('資訊')) & 
        (df['日間∕進修別'] == 'D 日') & 
        (df['等級別'].isin(['B 學士', 'B 大學', 'B 四技']))]

# Select the columns
df = df[['縣市名稱', '學校名稱', '科系名稱', '男生計', '女生計']]

# Write to HTML
df.to_html('111590003.html', index=False)
