# Import required libraries
import collections

# Read the file
with open('name.txt', 'r', encoding='utf-8') as f:
    names = f.read().split(', ')

# Extract surnames
surnames = [name.split()[0][0] for name in names]

# Count the occurrences of each surname
counter = collections.Counter(surnames)

# Sort the counter by the number of occurrences
sorted_surnames = sorted(counter.items(), key=lambda x: x[1], reverse=True)
print(surnames)
# Answer the questions
print(f"The surname that appears the most appears {sorted_surnames[0][1]} times.")
print(f"The surname that ranks 38th in terms of frequency is '{sorted_surnames[37][0]}''{sorted_surnames[37][1]}'.")
