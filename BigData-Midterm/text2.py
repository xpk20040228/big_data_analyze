def sum_numbers_in_file(file_path):
    with open(file_path, 'r') as file:
        lines = file.readlines()
    total = sum(int(line.strip()) for line in lines if line.strip().isdigit())
    return total

# Usage:
file_path = 'DonationNameList-2.txt'
print(sum_numbers_in_file(file_path))
