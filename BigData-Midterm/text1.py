import re

def clean_file(input_file, output_file):
    with open(input_file, 'r', encoding='utf-8') as f:
        lines = f.readlines()

    cleaned_lines = []
    for line in lines:
        cleaned_line = re.sub(r'[^0-9\n]', '', line)
        cleaned_lines.append(cleaned_line)

    with open(output_file, 'w', encoding='utf-8') as f:
        f.writelines(cleaned_lines)

# Usage
clean_file('DonationNameList-1.txt', 'DonationNameList-2.txt')
