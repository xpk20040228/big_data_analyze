import pandas as pd

# Read the CSV file
df = pd.read_csv('112.csv')

# Filter entries with '新北市'
df_new_taipei = df[df['縣市名稱'] == '新北市']

# Calculate the sum of the column '電子書(種)'
sum_ebooks = df_new_taipei['電子書(種)'].sum()

print(f"The sum of the column '電子書(種)' for entries with '新北市' is: {sum_ebooks}")
